create schema if not exists rentalstest collate utf8_unicode_ci;

create table _test
(
	id timestamp default CURRENT_TIMESTAMP not null
		primary key,
	success int not null
);

create table coupons
(
	coupon_id bigint not null
		primary key,
	value double default 1 not null,
	expired int default 0 not null
);

create table movies
(
	id bigint auto_increment
		primary key,
	title varchar(255) null,
	type varchar(255) null,
	year int null
);

create table users
(
	user_id bigint auto_increment
		primary key,
	email varchar(255) not null,
	password varchar(255) not null
);

create table points
(
	point_id bigint auto_increment
		primary key,
	user_id bigint not null,
	balance double not null,
	constraint points_ibfk_1
		foreign key (user_id) references users (user_id)
);

create index user_id
	on points (user_id);

create table rentals
(
	rental_id bigint auto_increment
		primary key,
	user_id bigint not null,
	total double not null,
	constraint rentals_ibfk_1
		foreign key (user_id) references users (user_id)
);

create table orders
(
	order_id bigint auto_increment
		primary key,
	rental_id bigint not null,
	movie_id bigint not null,
	pending_rtn int(1) not null,
	date_out datetime not null,
	date_rtn datetime not null,
	constraint orders_ibfk_1
		foreign key (rental_id) references rentals (rental_id),
	constraint orders_ibfk_2
		foreign key (movie_id) references movies (id)
);

create index movie_id
	on orders (movie_id);

create index rental_id
	on orders (rental_id);

create index user_id
	on rentals (user_id);

