#Python3.7 database.py
#Our fast DAO to Mysql connection

import pymysql
import configparser
import passwordUtil
import random
import string
import numpy as np 
import pandas as pd

config = configparser.ConfigParser()
config.read(".env.init")
db:any

# function to get unique values 
def unique(list1): 
    x = np.array(list1) 
    return(np.unique(x)) 

def randomString(stringLength=3):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

def get_conn():
    return  pymysql.connect(
                host=config.get("database","host"),
                user=config.get("database","username"),
                password=config.get("database","password"),
                db=config.get("database","database"),
                charset='utf8mb4',
                cursorclass=pymysql.cursors.DictCursor)


def executeManySqlAndData(sql,data,title:any=''):
    print('start:',title)
    try:
        # connect to mysql
        db = get_conn()
        # prepare a cursor object using cursor() method
        cursor = db.cursor()
        # Prepare each SQL query to INSERT a record into the database
        cursor.executemany(sql, data)
        print('success')
        db.commit()
    except Exception as e:
        print('error', e)
        # Rollback in case there is any error
        db.rollback()
    finally:
        #close connections
        cursor.close()
        db.close()

def executeSqlAndData(sql,data,title:any=''):
    print('start:',title)
    try:
        # connect to mysql
        db = get_conn()

        # prepare a cursor object using cursor() method
        cursor = db.cursor()
        # Prepare each SQL query to INSERT a record into the database
        cursor.execute(sql, data)
        print('success')
        db.commit()
    except Exception as e:
        print('error', e)
        # Rollback in case there is any error
        db.rollback()
    finally:
        #close connections
        cursor.close()
        db.close()

def executeSql(sql,title:any=''):
    print('start:',title)
    try:
        # connect to mysql
        db = get_conn()
        # prepare a cursor object using cursor() method
        cursor = db.cursor()
        # Prepare each SQL query to INSERT a record into the database
        cursor.execute(sql)
        
        print('success')
        db.commit()
    except Exception as e:
        print('error', e)
        # Rollback in case there is any error
        db.rollback()
    finally:
        #close connections
        cursor.close()
        db.close()

def init_orders_table():
    sql = "CREATE TABLE orders ( " \
            "order_id BIGINT(20) NOT NULL AUTO_INCREMENT, " \
            "rental_id BIGINT(20) NOT NULL, " \
            "movie_id BIGINT(20) NOT NULL, " \
            "pending_rtn INT(11) NOT NULL," \
            "date_out DATETIME COLLATE utf8_unicode_ci NOT NULL," \
            "date_rtn DATETIME COLLATE utf8_unicode_ci NOT NULL," \
            "PRIMARY KEY (order_id), " \
            "INDEX (rental_id)," \
            "FOREIGN KEY (rental_id) REFERENCES rentals(rental_id) " \
          " ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;" 
    executeSql(sql,'orders')
def init_test_table():
    sql = "create table _test"\
            "("\
                "id timestamp default CURRENT_TIMESTAMP not null"\
                "	primary key,"\
                "success int not null"\
            ");"
    executeSql(sql,'testtable')
def init_points_table():
    sql = "CREATE TABLE points ( " \
            "point_id BIGINT(20) NOT NULL AUTO_INCREMENT, " \
            "user_id BIGINT(20) NOT NULL, " \
            "balance DOUBLE  NOT NULL," \
            "PRIMARY KEY (point_id), " \
            "INDEX (user_id)," \
            "FOREIGN KEY (user_id) REFERENCES users(user_id) " \
          " ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;" 
    executeSql(sql,'points')

def init_coupons_table():
    sql = "CREATE TABLE coupons ( " \
            "coupon_id BIGINT(20) NOT NULL AUTO_INCREMENT, " \
            "value DOUBLE  NOT NULL," \
            "expired INT NOT NULL," \
            "PRIMARY KEY (coupon_id) " \
          " ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci  AUTO_INCREMENT=1 ;" 
    executeSql(sql,'coupon')

def init_default_coupons():
    # coupon_id, code, value, expired
    # sql = "INSERT INTO `coupons` (`coupon_id`, `value`, `expired`) VALUES (%s, %s, %s)"
    sql = "INSERT INTO `coupons` (`coupon_id`, `value`, `expired`) VALUES (%s,1,0)"
    _data:list = []
    i=0
    while i < 199:
        _data.append(int(random.randrange(100, 999, 3)))
        i = i+1
    data = unique(_data)
    my_list = [int(s) for s in data[1:-1]]
    executeManySqlAndData(sql, my_list, 'default coupons')

def init_default_test():
    sql = "INSERT INTO `_test`(`id`, `success`) VALUES (%s,%s)"
    # base = datetime.datetime.today()
    # date_list = [base - datetime.timedelta(days=x) for x in range(numdays)]
    datelist = pd.date_range(pd.datetime.today(), periods=100, freq='1m', tz='Europe/Madrid').tolist()
    my_list = []
    for key,s in enumerate(datelist[1:-1]):
        s = pd.Timestamp(s).to_pydatetime().strftime('%Y-%m-%d %H:%M:%S') #.isoformat(' ', 'milliseconds') #.strftime("%Y-%m-%d %H:%M:%S.%m").strftime('%Y-%m-%d %H:%M:%S')
        my_list.append([s, int(key)%2])
    executeManySqlAndData(sql, my_list, 'default test items')


def init_rentals_table():
    sql = "CREATE TABLE rentals ( " \
            "rental_id BIGINT(20) NOT NULL AUTO_INCREMENT, " \
            "user_id BIGINT(20) NOT NULL, " \
            "total DOUBLE NOT NULL," \
            "PRIMARY KEY (rental_id), " \
            "INDEX (user_id)," \
            "FOREIGN KEY (user_id) REFERENCES users(user_id) " \
          " ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;" 
    executeSql(sql,'rentals')

def init_users_table():
    sql = "CREATE TABLE users ( " \
            "user_id bigint(20) NOT NULL AUTO_INCREMENT, " \
            "email varchar(255) COLLATE utf8_unicode_ci NOT NULL, " \
            "password varchar(255) COLLATE utf8_unicode_ci NOT NULL," \
            "PRIMARY KEY (user_id) " \
          " ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;" 
    executeSql(sql,'users')

def init_default_user():
    sql = "INSERT INTO `users` (`email`, `password`) VALUES (%s, %s)"
    data = config.get("user","email"), passwordUtil.hash_password(config.get("user","password"))

    executeSqlAndData(sql,data,'default user')

def init_movies_table():
    sql = "CREATE TABLE movies (" \
        "movie_id bigint(20) NOT NULL AUTO_INCREMENT," \
        "title varchar(255) DEFAULT NULL," \
        "type varchar(255) DEFAULT NULL," \
        "year int DEFAULT NULL," \
        "PRIMARY KEY (movie_id)" \
    ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;"
    executeSql(sql,'movies')


def insert_movies(movies):
    query = "INSERT INTO movies(title,year,type) " \
             "VALUES(%s,%s,%s)"
    executeManySqlAndData(query, movies,'movies')
 

## database initialization scripts for test env / use with caution
# init_test_table()
# init_movies_table()
# init_users_table()
# init_default_user()
# init_orders_table()
# init_rentals_table()
# init_points_table()
# init_coupons_table()
# init_default_coupons()
# init_default_test()
