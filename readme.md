## Movie Rentals sub project initialisation ##



## What it does? ##
- ### Database creation and schema initialization ###
- ### Database new user ###
- ### Movie Scrapper ###
- ### Movie list filter, categorized and uploaded to database ###

## HOW TO RUN? ##
- Create a SQL database (Mysql in this case)
- Create a .env file
```[database]
host:170.0.0.80
port:3306
username:yourusername
database:yourdatabase
password:yourpassword

[user]
password:pass
email:user@singularcover.com

```
- Uncomment the database initialization scripts in database.py
```init_test_table()
init_movies_table()
init_users_table()
init_default_user()
init_orders_table()
init_rentals_table()
init_points_table()
init_coupons_table()
init_default_coupons()
init_default_test()
```
And simply execute the following command
`Python3 database.py`

Using python for convenience and faster development.


- Now that we have our database ready, let us move into the movie data population with the scrapper.
Start scrapper by typing in console
`Python3 scraper.py`


