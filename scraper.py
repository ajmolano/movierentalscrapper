# Python3.7 scraper.py
# Obtain a list of movies on each category and insert into database
# It is a manual execution script not intended for produciton use

from bs4 import BeautifulSoup
import requests
import re
import database as DAO

# List of sources with the proper html element to look for and the type of list:
premierMovieDataSource = 'https://www.imdb.com/movies-in-theaters/?ref_=nv_mv_inth','h4','premiere','a'
regularMovieDataSource = 'https://www.imdb.com/search/keyword/?keywords=american&ref_=kw_vw_adv&mode=advanced&page=1&sort=release_date,desc','h3','regular',['a', 'span']
regularMovieDataSource2 = 'https://www.imdb.com/search/keyword/?keywords=american&ref_=kw_vw_adv&mode=advanced&page=2&sort=release_date,desc','h3','regular',['a', 'span']
oldMovieDataSource =     'https://www.imdb.com/search/keyword/?keywords=american&ref_=kw_vw_adv&mode=advanced&page=1&sort=release_date,asc','h3','old',['a', 'span']
oldMovieDataSource2 =     'https://www.imdb.com/search/keyword/?keywords=american&ref_=kw_vw_adv&mode=advanced&page=2&sort=release_date,asc','h3','old',['a', 'span']
#build a list of sources
movieDataSourceList = premierMovieDataSource, regularMovieDataSource, oldMovieDataSource, regularMovieDataSource, oldMovieDataSource

myMovieDictArr = []

# prepare stands for data formating and cleaning
def prepareMyList(soupResult:any, movieType:any, movieTag:any='a'):
    for x in soupResult:
        #for y in x.find_all(movieTag):
        y = x.find_all(movieTag)
        match:any = None
        text:any
        year:any = None
        # premiere items have a different formating than others that need more filtering
        if (movieType != 'premiere'):
            text = x.select("a")
            if text is not None and len(text)>0:
                text = text[0]
                text = text.string
                if hasattr(text, 'find_next'):
                    _year = text.find_next('span', class_='lister-item-year text-muted unbold')

                    if hasattr(_year,'string'):
                        _year = _year.string
                        if _year is not None:
                            match = re.match(r'.*([1-3][0-9]{3})', _year)
                            if match is not None:
                                year = match.group(1)
        else:
            match = re.match(r'.*([1-3][0-9]{3})',y[0].text)
            text = y[0].text
            year = match.group(1)
        
        
        if year is not None:
            # myMovieDictArr.append({'title': text, 'type':movieType, 'year':year})
            myMovieDictArr.append([text, year, movieType])

# scraping method using soup library to download list of movies from an url
def getMovieTitlesListFromUrl(movieData:any):
    resp = requests.get(movieData[0])
    soup = BeautifulSoup(resp.text, features="lxml")
    return  prepareMyList(soup.find_all(movieData[1]),movieData[2],movieData[3])

# main process function - Start
def main():
    for m in movieDataSourceList:
        getMovieTitlesListFromUrl(m)
    # print(myMovieDictArr)
    DAO.insert_movies(myMovieDictArr)

 
if __name__ == '__main__':
    main()
